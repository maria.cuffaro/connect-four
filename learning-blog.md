# Learning Blog

- Modules - difference between node (commonjs) modules and browser (es) module syntax. Discovered that you can use es modules in node by setting the `type` property in `package.json`
- Keyboard Shortcuts -
- Testing - jasmine matchers.....
- Frameworks - decided to start from scratch without using any frameworks.
- Gitlab - setting up a project from scratch using git / gitlab.
- TDD - Red, Green, Refactor.
- BDD - User Story, then code.
- Pair programming - Driver / Navigator.
- Resources - Makers course repo.
- Javascript Classes - Object Oriented style javascript.
- CSS Grid
- Cypress e2e tests
- Express Server - routing / publishing static assets.
- Using Browser LocalStorage as a client side database.
- Serve backend code to client via server static/public folder.
- Create a gitlab pipeline from scratch to run our tests during merge
- Switched to the JL variable font

Leaning Goals for the future:

- Server side database?
- Authentication / Login.
- Cloud based.
- New frameworkd (Svelte, Remix)
- Showcase to others?
- Typescript.
- Replace jasmine with ????
- Performance Testing
- Accessibility Testing
- APIs
- App?? (React Native)
- WebAPIs (WebGL? )

Lessons Learnt:

- By deciding to do TDD we were able to create complex game logic and refactor it many times without anything breaking.

- By not using frameworks, bundlers compilers or 3rd party code the process of creating a website and building it was demysified

- We learnt vanilla javascript, css and html with no framework or syntactic sugar that we are used to in our day jobs.

- Maybe next time we could have tried some different styles of pair programming.

- Perhaps given more time we could have showcased to the other partner devs (there is still time for that...)

- It is possible to work 100% remotely

- We did use frameworks for unit testing and end to end visual testing - we could have written our own.

- Pairing and booking a regular slot makes it happen as you are accountable to each other, although flexibility is important.

- Building a server is easy!

- Chatgpt is scarily knowledgable

- pairing on side projects helps build relationships with other devs and creates a professional network
