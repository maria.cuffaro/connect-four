# Purpose of this Project

A pairing project to create a connect four game using javascript, tdd, pair programming

# User Stories

## Story 1

As a user, I want to start a game of connect 4, so that I can play.

## Story 2

As a user, I want register my name, so that I can record my high score later.

## Story 3

As a user, I want to see an empty 6x7 grid, so that I know what the grid looks like.

## Story 4

As a user, I want to make a move.

## Story 5

As a user, when I enter a move that is outside of the grid, the grid will not update.

## Story 6

As a user, if I've just made an illegal move, when I display the grid it tells me.

## Story 7

As a user, if I've just made an illegal move, when I display the grid it tells me, but subsequently lets me make a legal move.

## Story 8

As a user, if I get 4 in a row I win the game.

## Story 9

As a user, if I get 4 in a column I win the game.

## Story 10

As a user, if I get 4 in a diagonal I win the game.

    ## Story 11

As a user, I can enter user names onto a Welcome Screen

## Story 12

As a user, once I have entered player names I want to see the start grid

## Story 13

As a user, once I have entered player names I want to see the start grid

# Testing

# Install cypress

```console
npx cypress install
```

Troubleshooting cypress - try the following

```console
npx cypress cache clear
npx cypress install
npx cypress open
```

## Unit tests

To run the test suite

```console
npm test
```

## e2e Tests

```console
npm run e2e
```

# Screen Designs

https://jamboard.google.com/d/1uoaiFjKJa3lOG-V8ISfTXRHNZVDkxcgouVgGD7zuk3o/edit?usp=sharing

# Start (express node) server

```console
npm start
```

# Display Hello Page

```console
npm start
```

then navigate to https://localhost:3001/hello.html
