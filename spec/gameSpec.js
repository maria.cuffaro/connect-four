import Game from '../src/game.js';

describe('new game', () => {
  it('should start', () => {
    const game = new Game();
    expect(game.start()).toBe('Game has started');
    expect(game).toBeInstanceOf(Game);
  });
  it('should allow me to register 2 players names when I start a game', () => {
    const game = new Game('james','maria');
    expect(game.name1).toBe('james');
    expect(game.name2).toBe('maria');
  });
  it('should default to player1 and player2', () => {
    const game = new Game();
    expect(game.name1).toBe('player1');
    expect(game.name2).toBe('player2');
  });
  it('should contain an empty 7x6 grid', () => {
    const game = new Game();
    expect(game.grid).toEqual([[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0]]);
  });
  it('should display the starting grid', () => {
    const game = new Game();
    expect(game.display()).toEqual("\nWelcome\n0 0 0 0 0 0 0\n0 0 0 0 0 0 0\n0 0 0 0 0 0 0\n0 0 0 0 0 0 0\n0 0 0 0 0 0 0\n0 0 0 0 0 0 0\nPlease make your move");
  });
});

describe('playing the game', () => {
  let game;
  
  beforeEach(() => {
    game = new Game();
  });

  it('should allow to drop their tokens and record their move', () => {
    game.move(1);
    expect(game.display()).toEqual("\n0 0 0 0 0 0 0\n0 0 0 0 0 0 0\n0 0 0 0 0 0 0\n0 0 0 0 0 0 0\n0 0 0 0 0 0 0\nX 0 0 0 0 0 0\nPlease make your move");
    game.move(2);
    expect(game.display()).toEqual("\n0 0 0 0 0 0 0\n0 0 0 0 0 0 0\n0 0 0 0 0 0 0\n0 0 0 0 0 0 0\n0 0 0 0 0 0 0\nX Y 0 0 0 0 0\nPlease make your move");
    game.move(1);
    expect(game.display()).toEqual("\n0 0 0 0 0 0 0\n0 0 0 0 0 0 0\n0 0 0 0 0 0 0\n0 0 0 0 0 0 0\nX 0 0 0 0 0 0\nX Y 0 0 0 0 0\nPlease make your move");
    game.move(2);
    expect(game.display()).toEqual("\n0 0 0 0 0 0 0\n0 0 0 0 0 0 0\n0 0 0 0 0 0 0\n0 0 0 0 0 0 0\nX Y 0 0 0 0 0\nX Y 0 0 0 0 0\nPlease make your move");
    game.move(7);
    expect(game.display()).toEqual("\n0 0 0 0 0 0 0\n0 0 0 0 0 0 0\n0 0 0 0 0 0 0\n0 0 0 0 0 0 0\nX Y 0 0 0 0 0\nX Y 0 0 0 0 X\nPlease make your move");
    expect(game.move.bind(game, 1)).not.toThrowError('Illegal column number');
    expect(game.move.bind(game, 7)).not.toThrowError('Illegal column number');
  }); 

  it('should not allow a move into any columns outside of the grid', () => {
    expect(game.move.bind(game, 8)).toThrowError('Illegal column number');
    expect(game.move.bind(game, 0)).toThrowError('Illegal column number');
  }); 
  
  it('should fill a column', () => {
    game.move(1);
    game.move(1);
    game.move(1);
    game.move(1);
    game.move(1);
    game.move(1);
    expect(game.display()).toEqual("\nY 0 0 0 0 0 0\nX 0 0 0 0 0 0\nY 0 0 0 0 0 0\nX 0 0 0 0 0 0\nY 0 0 0 0 0 0\nX 0 0 0 0 0 0\nPlease make your move");
  });

  it('should not allow a move into a full column', () => {
    game.move(1);
    game.move(1);
    game.move(1);
    game.move(1);
    game.move(1);
    game.move(1);
    expect(game.display()).toEqual("\nY 0 0 0 0 0 0\nX 0 0 0 0 0 0\nY 0 0 0 0 0 0\nX 0 0 0 0 0 0\nY 0 0 0 0 0 0\nX 0 0 0 0 0 0\nPlease make your move");
    game.move(1);
    expect(game.display()).toEqual("Illegal move, Please try again.");
  });

  it('should not allow a move into a full column, but then allow a further legal move', () => {
    game.move(1);
    game.move(1);
    game.move(1);
    game.move(1);
    game.move(1);
    game.move(1);
    expect(game.display()).toEqual("\nY 0 0 0 0 0 0\nX 0 0 0 0 0 0\nY 0 0 0 0 0 0\nX 0 0 0 0 0 0\nY 0 0 0 0 0 0\nX 0 0 0 0 0 0\nPlease make your move");
    game.move(1);
    expect(game.display()).toEqual("Illegal move, Please try again.");
    game.move(2);
    expect(game.display()).toEqual("\nY 0 0 0 0 0 0\nX 0 0 0 0 0 0\nY 0 0 0 0 0 0\nX 0 0 0 0 0 0\nY 0 0 0 0 0 0\nX X 0 0 0 0 0\nPlease make your move");
  });

});

describe('winning the game', () => {
  let game;
  
  beforeEach(() => {
    game = new Game();
  });

  it('should print a message when someone wins the game', () => {
    const winningGrid = [[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],['Y','Y','Y',0,0,0,0],['X','X','X','X',0,0,0]]
    expect(game.hasWon(winningGrid)).toBeTrue();
  });
  
  it('wins the game with a vertical row', () => {
    const winningGrid = [[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],['X',0,0,0,0,0,0],['X','Y',0,0,0,0,0],['X','Y',0,0,0,0,0],['X','Y',0,0,0,0,0]]
    expect(game.hasWon(winningGrid)).toBeTrue();
  });

  it('does not register a win unless there are 4 in a column', () => {
    const winningGrid = [[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],['X','Y',0,0,0,0,0],['X','Y',0,0,0,0,0],['X','Y',0,0,0,0,0]]
    expect(game.hasWon(winningGrid)).toBeFalse();
  });

  it('does not register a win unless there are 4 in a row', () => {
    const winningGrid = [[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],['Y','Y','Y',0,0,0,0],['X','X','X',0,0,0,0]]
    expect(game.hasWon(winningGrid)).toBeFalse();
  });

  it('does register a win if there are 4 in a diagonal starting top left', () => {
    const winningGrid = [
      ['X',0,0,0,0,0,0],
      ['Y','X',0,0,0,0,0],
      ['X','Y','X',0,0,0,0],
      ['Y','Y','X','X',0,0,0],
      ['Y','X','Y','Y',0,0,0],
      ['Y','X','X','Y',0,0,0]
    ]
    expect(game.testMatrixForDiagonalWin(winningGrid)).toBeTrue();
    expect(game.hasWon(winningGrid)).toBeTrue();
  });

  it('does register a win if there are 4 in a diagonal', () => {
    const winningGrid = [
      ['Y','Y',0,0,0,0,0],
      ['Y','X','Y',0,0,0,0],
      ['X','Y','X','Y',0,0,0],
      ['Y','Y','X','X','Y',0,0],
      ['Y','X','Y','Y','Y',0,0],
      ['Y','X','X','Y','X',0,0]
    ]
    expect(game.testMatrixForDiagonalWin(winningGrid)).toBeTrue();
    expect(game.hasWon(winningGrid)).toBeTrue();
  });

  it('does register a win if there are 4 in a diagonal starting on row 2', () => {
    const winningGrid = [
      ['Y','Y',0,0,0,0,0],
      ['Y','Y','X',0,0,0,0],
      ['X','Y','X','Y',0,0,0],
      ['Y','Y','Y','Y','Y',0,0],
      ['Y','X','Y','Y','Y',0,0],
      ['Y','X','X','Y','X',0,0]
    ]
    expect(game.testMatrixForDiagonalWin(winningGrid)).toBeTrue();
    expect(game.hasWon(winningGrid)).toBeTrue();
  });

  it('does register a win if there are 4 in a diagonal starting on row 3', () => {
    const winningGrid = [
      ['Y','Y',0,0,0,0,0],
      ['X','Y','X',0,0,0,0],
      ['X','Y','X','Y',0,0,0],
      ['Y','Y','Y','Y','Y',0,0],
      ['Y','X','Y','Y','Y',0,0],
      ['Y','X','X','Y','Y',0,0]
    ]
    expect(game.testMatrixForDiagonalWin(winningGrid)).toBeTrue();
    expect(game.hasWon(winningGrid)).toBeTrue();
  });

  it('does not register a win for an empty grid', () => {
    const emptyGrid = [
      [0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0]
    ]
    expect(game.testMatrixForDiagonalWin(emptyGrid)).toBeFalse();
    expect(game.hasWon(emptyGrid)).toBeFalse();
  });

  it('does not register a win for 3 in a diagonal', () => {
    const winningGrid = [
      ['X',0,0,0,0,0,0],
      ['Y','X',0,0,0,0,0],
      ['X','Y','X',0,0,0,0],
      ['Y','Y','X',0,0,0,0],
      ['X','Y','Y',0,0,0,0],
      ['Y','X','X',0,0,0,0]
    ]
    expect(game.testMatrixForDiagonalWin(winningGrid)).toBeFalse();
    expect(game.hasWon(winningGrid)).toBeFalse();

  });
  it('does register a win with a diagonal in opposite direction starting on row 1', () => {
    const winningGrid = [
      [0,0,0,'X',0,0,0],
      [0,0,'X','Y',0,0,0],
      [0,'X','X','X',0,0,0],
      ['X','Y','Y','Y',0,0,0],
      ['X','X','Y','Y','Y','X','Y'],
      ['X','Y','X','Y','X','Y','X'],
    ]
    expect(game.testMatrixForDiagonalWin(winningGrid)).toBeTrue();
    expect(game.hasWon(winningGrid)).toBeTrue();
  });
  it('does register a win with a diagonal in opposite direction starting on row 2', () => {
    const winningGrid = [
      [0,0,0,0,0,0,0],
      [0,0,'X','Y',0,0,0],
      [0,'X','Y','X',0,0,0],
      ['X','Y','Y','Y',0,0,0],
      ['Y','X','Y','Y','Y','X','Y'],
      ['X','Y','X','Y','X','Y','X'],
    ]
    expect(game.testMatrixForDiagonalWin(winningGrid)).toBeTrue();
    expect(game.hasWon(winningGrid)).toBeTrue();
  });
  it('does register a win with a diagonal in opposite direction starting on row 3', () => {
    const winningGrid = [
      [0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0],
      [0,0,0,'X',0,0,0],
      [0,0,'X','Y',0,0,0],
      [0,'X','Y','X',0,0,0],
      ['X','Y','Y','Y',0,0,0]
    ]
    expect(game.testMatrixForDiagonalWin(winningGrid)).toBeTrue();
    expect(game.hasWon(winningGrid)).toBeTrue();
  });
  it('does register a win with a diagonal in opposite direction starting on row 3, column 7', () => {
    const winningGrid = [
      [0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0],
      ['X','X','Y','X','Y','X','Y'],
      ['X','Y','Y','X','Y','Y','X'],
      ['Y','X','Y','Y','Y','X','Y'],
      ['X','Y','X','Y','X','Y','X'],
    ]
    expect(game.testMatrixForDiagonalWin(winningGrid)).toBeTrue();
    expect(game.hasWon(winningGrid)).toBeTrue();
  });
  it('does not register a win with a random mix of X and Y', () => {
    const winningGrid = [
      ['X','Y','X','Y','X','Y','X'],
      ['X','Y','X','Y','X','Y','X'],
      ['Y','X','Y','X','Y','X','Y'],
      ['Y','X','Y','X','Y','X','Y'],
      ['X','Y','X','Y','X','Y','X'],
      ['X','Y','X','Y','X','Y','X'],
    ]
    expect(game.testMatrixForDiagonalWin(winningGrid)).toBeFalse();
    expect(game.hasWon(winningGrid)).toBeFalse();
  });
  it('does not register any further moves once the game is won', () => {
    game.move(1);
    game.move(2);
    game.move(1);
    game.move(2);
    game.move(1);
    game.move(2);
    game.move(1);
    expect(game.display()).toEqual("\n0 0 0 0 0 0 0\n0 0 0 0 0 0 0\nX 0 0 0 0 0 0\nX Y 0 0 0 0 0\nX Y 0 0 0 0 0\nX Y 0 0 0 0 0\nplayer1 Wins!");
    game.move(1);
    expect(game.display()).toEqual("Game Over");
  })
  it('does registers player 2 has won', () => {
    game.move(1);
    game.move(2);
    game.move(1);
    game.move(2);
    game.move(1);
    game.move(2);
    game.move(3);
    game.move(2);
    expect(game.display()).toEqual("\n0 0 0 0 0 0 0\n0 0 0 0 0 0 0\n0 Y 0 0 0 0 0\nX Y 0 0 0 0 0\nX Y 0 0 0 0 0\nX Y X 0 0 0 0\nplayer2 Wins!");
    game.move(1);
    expect(game.display()).toEqual("Game Over");
  });
  it('uses the entered name of a player if there is one', () => {
    const gameWithNamedPlayers = new Game('james','maria');
    gameWithNamedPlayers.move(1);
    gameWithNamedPlayers.move(2);
    gameWithNamedPlayers.move(1);
    gameWithNamedPlayers.move(2);
    gameWithNamedPlayers.move(1);
    gameWithNamedPlayers.move(2);
    gameWithNamedPlayers.move(1);
    expect(gameWithNamedPlayers.display()).toEqual("\n0 0 0 0 0 0 0\n0 0 0 0 0 0 0\nX 0 0 0 0 0 0\nX Y 0 0 0 0 0\nX Y 0 0 0 0 0\nX Y 0 0 0 0 0\njames Wins!");
    gameWithNamedPlayers.move(1);
    expect(gameWithNamedPlayers.display()).toEqual("Game Over");
  });
});
