const login = () => {
  cy.visit('http://localhost:3001/');
  cy.get('[data-testid="player1-name"]').type('Player1');
  cy.get('[data-testid="player2-name"]').type('Player2');
  cy.get('input[type="submit"]').click();
};

const onePlayerLogin = () => {
  cy.visit('http://localhost:3001/');
  cy.get('[data-testid="player1-name"]').type('Player1');
  cy.get('input[type="submit"]').click();
};

describe('Welcome Page', () => {
  it('shows the welcome page with the correct fields', () => {
    cy.visit('http://localhost:3001/');
    cy.get('[data-testid="title"]').should('have.text','Welcome');
    cy.get('[data-testid="player1-label"]').should('have.text','Player 1');
    cy.get('[data-testid="player2-label"]').should('have.text','Player 2');
    cy.get('input[type="submit"]').should('have.value','Start');
  })
  it('shows an error if no player names entered', () => {
    cy.visit('http://localhost:3001/');  
    cy.get('input[type="submit"]').click();
    cy.get('[id="error-message"]').should('have.text', 'Please enter player names')
    cy.get('[data-testid="player2-name"]').type('Player2');
    cy.get('input[type="submit"]').click();
    cy.get('[id="error-message"]').should('have.text', 'Please enter player names')
  })
})
describe('Game Page', () => {
  it('shows an error if no player names entered', () => {
    login();
    // check that we have redirected to the game page
    cy.url().should('eq', 'http://localhost:3001/game/');
    cy.get('[data-testid="game-container"]').should('exist');
  })
  it('shows message that game has started, and player names', () => {
    login();
    // check that we have redirected to the game page
    cy.url().should('eq', 'http://localhost:3001/game/');
    cy.get('[data-testid="game-message"]').should('exist');
    cy.get('[data-testid="game-message"]').contains('Game has started');
  })
  it('allows player 1 to make a move', () => {
    login();
    // check that we have redirected to the game page
    cy.url().should('eq', 'http://localhost:3001/game/');
    cy.get('[data-testid="game-message"]').should('exist');
    cy.get('[data-testid="game-message"]').contains('Game has started');
    const row1col1 = cy.get('[data-testid="game:grid:row1:col1"]');
    row1col1.click();
    const row1col1Token = cy.get('[data-testid="token-game:grid:row1:col1"]');
    expect(row1col1Token.should('have.class',"red"));
    expect(row1col1Token.should('not.have.class',"empty"));
  })
  it('allows player 2 to make a move', () => {
    login();
    // check that we have redirected to the game page
    cy.url().should('eq', 'http://localhost:3001/game/');
    cy.get('[data-testid="game-message"]').should('exist');
    cy.get('[data-testid="game-message"]').contains('Game has started');
    const row1col1 = cy.get('[data-testid="game:grid:row1:col1"]');
    row1col1.click();
    const row1col1Token = cy.get('[data-testid="token-game:grid:row1:col1"]');
    expect(row1col1Token.should('have.class',"red"));
    expect(row1col1Token.should('not.have.class',"empty"));
    const row1col2 = cy.get('[data-testid="game:grid:row1:col2"]');
    row1col2.click();
    const row1col2Token = cy.get('[data-testid="token-game:grid:row1:col2"]');
    expect(row1col2Token.should('have.class',"yellow"));
    expect(row1col2Token.should('not.have.class',"empty"));
  })
  it('puts token in correct row if dropped from top of column', () => {
    login();
    // check that we have redirected to the game page
    cy.url().should('eq', 'http://localhost:3001/game/');
    cy.get('[data-testid="game-message"]').should('exist');
    cy.get('[data-testid="game-message"]').contains('Game has started');
    const row6col1 = cy.get('[data-testid="game:grid:row6:col1"]');
    row6col1.click();

    const row6col1Token = cy.get('[data-testid="token-game:grid:row6:col1"]');
    const row1col1Token = cy.get('[data-testid="token-game:grid:row1:col1"]');
    cy.wait(500);

    expect(row1col1Token.should('have.class',"red"));
    expect(row1col1Token.should('not.have.class',"empty"));
    expect(row6col1Token.should('not.have.class',"red"));
  })
  it('starts a one player game', () => {
    onePlayerLogin();
    cy.url().should('eq', 'http://localhost:3001/game/');
    //extra visit needed to sub Math.random, otherwise lost on click
    cy.visit('http://localhost:3001/game',{
        onBeforeLoad(win) {
          cy.stub(win.Math,"random").returns(0.05);
        },
      },
    );
    cy.get('[data-testid="game-name-player1"]').contains('Player1');
    cy.get('[data-testid="game-name-player2"]').contains('Robbie the Robot');
    cy.get('[data-testid="game-message"]').contains('Game has started');
    const row6col1 = cy.get('[data-testid="game:grid:row6:col1"]');
    row6col1.click();
    const row2col1Token = cy.get('[data-testid="token-game:grid:row2:col1"]');
    const row1col1Token = cy.get('[data-testid="token-game:grid:row1:col1"]');
    cy.wait(500);
    expect(row1col1Token.should('have.class',"red"));
    expect(row1col1Token.should('not.have.class',"empty"));
    //check robot has moved
    expect(row2col1Token.should('have.class',"yellow"));
    expect(row2col1Token.should('not.have.class',"empty"));
  })
})