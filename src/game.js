function transpose(matrix) {
  const rows = matrix.length, cols = matrix[0].length;
  const grid = [];
  for (let j = 0; j < cols; j++) {
    grid[j] = Array(rows);
  }
  for (let i = 0; i < rows; i++) {
    for (let j = 0; j < cols; j++) {
      grid[j][i] = matrix[i][j];
    }
  }
  return grid;
}

export default class Game {
  constructor(name1 = 'player1', name2 = 'player2'){
    this.name1 = name1;
    this.name2 = name2;
    this.grid = [[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0],[0,0,0,0,0,0,0]]
    this.moveNumber = 0;
    this.errorMessage = undefined;
    this.gameOver = false;
  }
  start(){
    return 'Game has started';
  }

  testMatrixForWin(grid){
    let result = false;
    grid.forEach((row,)=>{
      row.forEach((element, elementIndex)=>{
        if(elementIndex <=3 && element !== 0){
          if(element === row[elementIndex + 1]) {
            if(element === row[elementIndex + 2]){
              if(element === row[elementIndex + 3]){
                result = true
              }
            }
          } 
        }
      })
    })
    return result;
  }

  testMatrixForDiagonalWin(grid){
    let result = false;

    for(let j=0; j<3; j++) {
      for(let i=0; i<3; i++) {
        const startElement = grid[j][i];
        const element1 = grid[j+1][i+1];
        const element2 = grid[j+2][i+2];
        const element3 = grid[j+3][i+3];
    
        if (startElement === element1 && startElement === element2 && startElement === element3) { 
          if (startElement !== 0){
            result = true; 
            break; 
          } 
        };
      };
      for(let i=3; i<7; i++) {
        const startElement = grid[j][i];
        const element1 = grid[j+1][i-1];
        const element2 = grid[j+2][i-2];
        const element3 = grid[j+3][i-3];
    
        if (startElement === element1 && startElement === element2 && startElement === element3) { 
          if (startElement !== 0){
            result = true; 
            break; 
          } 
        };
      };
    };
    return result;
  }

  hasWon(grid=this.grid){
    let result = false;
    result = this.testMatrixForWin(grid);

    if (result === false) {
      
      const transposedGrid = transpose(grid);
      result = this.testMatrixForWin(transposedGrid);
    }

    if (result === false) {
      result = this.testMatrixForDiagonalWin(grid);
    }
    
    if (result === true) {
      console.log('set game over');
      this.gameOver = true;
    }
    return result
  }

  display(){
    let result = '';
    this.grid.forEach(element => {
      result =  `${result}\n${element.join(' ')}`
    });
    if (this.errorMessage) {
      const returnMessage = this.errorMessage;
      this.errorMessage = undefined;
      return returnMessage;
    }
    if (this.hasWon()) {
      const winner = this.moveNumber%2 === 0 ? this.name2 : this.name1
      return `${result}\n${winner} Wins!`;
    }
    return `${this.moveNumber===0 ? '\nWelcome' : ''}${result}\nPlease make your move`;
  }

  move(columnNumber){
    if (this.gameOver){
      this.errorMessage = 'Game Over'
      return
    }
    if (columnNumber > 7 || columnNumber < 1 ) {
      throw new Error('Illegal column number')
    }

    const playerSymbol = this.moveNumber % 2 === 0 ? 'X' : 'Y';
    
    let newGrid = this.grid;
    let isMoveComplete = false;
    
    const isLastRow = (rowIndex) => rowIndex === 5;
    const isSpaceEmptyForChosenColumn = (row, columnNumber) => row[columnNumber-1] === 0;
    const placeToken = (grid, rowIndex, columnNumber, playerSymbol) => grid[rowIndex][columnNumber-1] = playerSymbol;
    const isSpaceBelowFull = (grid, rowIndex, columnNumber) => (grid[rowIndex+1][columnNumber-1] !==0);
    
    // if there is already a token in the top slot
    if (this.grid[0][columnNumber-1] != 0) {
      this.errorMessage = 'Illegal move, Please try again.';
      return;
    }

    this.grid.forEach((row, rowIndex) => {
      if (!isMoveComplete) {
        if (isLastRow(rowIndex) && isSpaceEmptyForChosenColumn(row, columnNumber)) {
          placeToken(newGrid, rowIndex, columnNumber, playerSymbol);
        } else {
          if (isSpaceBelowFull(newGrid, rowIndex, columnNumber)) {
            placeToken(newGrid, rowIndex, columnNumber, playerSymbol);
            isMoveComplete = true;
          } else {
            newGrid[rowIndex] = row
          }
        }
      }
    });
        
    this.grid = newGrid;
    this.moveNumber++;
  }
}
